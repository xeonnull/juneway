#!/bin/bash
PROJECT_ID="25390175"
curl --header "Authorization: Bearer $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs?scope=failed" | jq '[.[]|select(.name=="build_dev_pr")][0]'
